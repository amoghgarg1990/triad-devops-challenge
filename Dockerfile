FROM python:3.7-alpine

RUN pip install mkdocs --no-cache-dir
RUN mkdir /app

COPY ./entrypoint.sh /app
ENTRYPOINT ["/app/entrypoint.sh"]
