# TRI-AD Devops challenge

This repo contains a solution to the devops challenge as described in the `./DevOps_Challenge.pdf` document.

## Build docker image
Note that the image is called `mkdocs`.
```
cd /path/to/this/repo
docker build -t mkdocs .
```

## Using the image via helper script

You can use the docker image with the help of the included helper script.     
A mock valid mkdocs directory can be created by `mkdocs new .` command. For convenience I have included such a directory at `./test`.

##### Produce a compressed built mkdocs site (tgz) to stdout
```
# cd /path/to/valid/mkdocs-directory
cd test
../mkdockerize.sh produce
```

##### Serve the docs on localhost:8000
```
cd test
../mkdockerize.sh produce | ../mkdockerize.sh serve
```


## Using the docker image directly

##### Produce a compressed built mkdocs site (tgz) to stdout
Mount the docs directory to `/docs_source` inside the container.  
```
cd /path/to/valid/mkdocs-directory
docker run -v `pwd`:/docs_source mkdocs produce
```

##### Serve the docs on localhost:8000
```
cd /path/to/valid/mkdocs-directory
docker run -v `pwd`:/docs_source mkdocs produce > /tmp/archive.tgz
cat /tmp/archive.tgz | docker run -i -p 8000:8000 mkdocs serve
```

# Some notes

1. I have interpreted `Write out to the ​ stdout​ a ​ .tar.gz​ file.` as writing out
the binary contents of the archive file to stdout.
1. Since the challenge specifies to use Mkdocs to serve the website, **I have included
the original source of the docs in the tar archive.** This is because as far as I could understand,
Mkdocs serves only from source and not a built static site.
1. For the Jenkins test, I have included a mock docs source in `./test` directory since it is
only two small text files. Other option was to assume mkdocs is present in Jenkins environment to
run the `mkdocs new <dir>` command.
1. I have tested the Jenkinsfile on a Jenkins system running on an Ubuntu machine.
1. I have tested the Dockerfile on docker version 19.03
