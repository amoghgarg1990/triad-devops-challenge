#!/bin/sh -e

IMAGE_NAME=mkdocs

produce() {
  docker run --rm -v `pwd`:/docs_source $IMAGE_NAME produce
}

serve() {
  docker run --rm -i -p 8000:8000 $IMAGE_NAME serve
}

usage() {
  echo "Usage: $0 COMMAND"
  echo ""
  echo "Commands:"
  echo "  produce \t write tz archive of a mkdocs site to stdout. Should be run in a valid mkdocs directory."
  echo "  serve   \t read the produce output from stdin and serve it on 0.0.0.0:8000"
}

case "$*" in
    serve) serve
        ;;
    produce) produce
        ;;
    *) usage
       exit 1
        ;;
esac

exit 0
