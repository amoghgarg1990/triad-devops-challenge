#!/bin/sh -e

produce() {
  cd /docs_source
  mkdocs build --clean --site-dir /tmp/mkdocs-site
  # Copy the source to the archive so that serve command can use it for serving the site
  cp -r . /tmp/mkdocs-site/__source
  tar -czf - -C /tmp/mkdocs-site . | cat
}

serve() {
  # Serve command expects a valid tgz file in stdin. It also expects a valid
  # mkdocs directory in ./__source folder in the tar file.
  mkdir -p /tmp/__archive
  cd /tmp/__archive
  tar -xzf -
  cd /tmp/__archive/__source && mkdocs serve --dev-addr 0.0.0.0:8000
}

usage(){
  echo "Usage: $0 COMMAND"
  echo ""
  echo "Commands:"
  echo "  produce \t write tz archive of a mkdocs site to stdout. Mount the docs to /docs_source."
  echo "  serve   \t read the produce (tar file) output from stdin, serve it on 0.0.0.0:8000."
}

case "$*" in
    serve) serve
        ;;
    produce) produce
        ;;
    *) usage
       exit 1
        ;;
esac

exit 0
